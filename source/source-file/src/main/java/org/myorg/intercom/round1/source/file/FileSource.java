package org.myorg.intercom.round1.source.file;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.google.inject.name.Named;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.myorg.intercom.round1.utils.Customer;
import org.myorg.intercom.round1.source.api.Source;

/**
 * 
 * @author jarvis
 *
 */
@Singleton
public class FileSource implements Source {

	private final static Logger logger = LoggerFactory.getLogger(FileSource.class);

	// Maps each line of input file to a Customer record
	@Inject
	private ObjectMapper objectMapper;

	// Property that defines the source data path
	@Inject
	@Named("source_data_url")
	private String sourceURL;

	// Property that defines the location of corrupt data file
	@Inject
	@Named("corrupt_data_dump_path")
	private String corruptDataDumpPath;

	private Stream<String> recordsBatch;
	private ArrayList<String> corruptRecords;

	// Perform initial setup needed before the processing starts
	@Override
	public void setup() throws IOException {
		try {
			logger.info("Connecting to the source '{}'", sourceURL);
			recordsBatch = Files.lines(Paths.get(sourceURL));
		} catch (IOException e) {
			logger.error("Exception while trying to connect to data source", e);
			throw e;
		}
		corruptRecords = new ArrayList<>();
	}

	/**
	 * @throws IOException
	 * @throws SourceNotFoundException
	 * 
	 */
	@Override
	public ArrayList<Customer> read() throws IOException {
		this.setup();
		logger.info("Reading customer data from the source");
		ArrayList<Customer> customerData = new ArrayList<>();
		recordsBatch.forEach(record -> {
			try {
				customerData.add(objectMapper.readValue(record, Customer.class));
			} catch (IOException e) {
				// Swallow the exception for this record and move on. Log the corrupt record
				// into the corrupt data dump file instead of printing it here to make sure data
				// privacy is not violated. This also allows users to debug data issues later
				corruptRecords.add(record + " | CAUSE : " + e.getLocalizedMessage().split("\n")[0]);
			}
		});
		this.clean();
		return customerData;
	}

	/**
	 * @throws IOException
	 * 
	 */
	@Override
	public void clean() throws IOException {
		logger.info("Performing resuorce cleanup");
		try {
			if (!corruptRecords.isEmpty())
				Files.write(Paths.get(corruptDataDumpPath + "corrupt_data.log"), corruptRecords);
			if (recordsBatch != null)
				recordsBatch.close();
		} catch (IOException e) {
			logger.error("Exception during cleanup", e);
			throw e;
		}
	}
}