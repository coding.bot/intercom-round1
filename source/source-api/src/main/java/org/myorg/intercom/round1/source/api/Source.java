package org.myorg.intercom.round1.source.api;

import java.io.IOException;
import java.util.ArrayList;

import org.myorg.intercom.round1.utils.Customer;

public interface Source {

	public void setup() throws IOException;

	public ArrayList<Customer> read() throws IOException;

	public void clean() throws IOException;
}