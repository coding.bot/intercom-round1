package org.myorg.intercom.round1.sink.console;

import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.myorg.intercom.round1.sink.api.Sink;

/**
 * 
 * @author jarvis
 *
 */
public class ConsoleSink implements Sink {

	private final static Logger logger = LoggerFactory.getLogger(ConsoleSink.class);

	/**
	 * 
	 */
	@Override
	public void setup() {

	}

	/**
	 * 
	 * @param dateToWrite
	 */
	@Override
	public void write(TreeMap<Integer, String> dateToWrite) {
		logger.info("Writing output to console sink");
		System.out.println("\nUsers who are close by and can be invited :\nID\t:\tName\n");
		dateToWrite.forEach((key, value) -> {
			System.out.println(key + "\t:\t" + value);
		});
	}

	/**
	 * 
	 */
	@Override
	public void clean() {
		// TODO Auto-generated method stub

	}
}