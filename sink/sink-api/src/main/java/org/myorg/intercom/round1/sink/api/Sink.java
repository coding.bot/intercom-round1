package org.myorg.intercom.round1.sink.api;

import java.util.TreeMap;

public interface Sink {

	public void setup();

	public void write(TreeMap<Integer, String> dateToWrite);

	public void clean();
}