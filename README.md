# intercom-round1

The intercom-round1 project is the implementation of use case assigned to me as part of the first round of Intercom's interview process.

At its core the intercom-round1 project solves the problem of reading customer data from a source and finding out the users who fall within 100 kms from the Intercom Dublin office. The output then gets written into the sink

** This is a work in progress. Javadoc and unit tests along with some other enhancements are still pending.

## Project structure

The project comprises of following modules -

### 1. Application :

The application module serves as the main entry point of the project. This module also contains the code that wires interfaces and their implementation through dependency injection using Google Guice DI framework.

### 2. Commons : 
Commons module is the placeholder for a set of utility classes that can be used across the project. They serve some of the trivial jobs like finding the distance, loading properties etc.

### 3. Source : 
The source module is an interface based component of the project that allows us to read input data from a specific source. For the purpose of this project we are reading data from a text file, called customers.txt. To achieve this we have a sub module, called source-file that implements source-api interface. The abstraction of source-api along with it's implementation, source-file here, is organized as sub modules to make it easier for the users of this API to introduce new custom sources in future. 

### For example,
If we need data to be read from a database source in future then we just need to create another sub module, say source-db, inside the source module. This will make sure that we are not tampering with the existing top level project structure. Whatever happens remains inside the source module. It's just a matter of adding a new folder inside source module.

### 4. Sink :
The sink module is an interface based component of the project that allows us to write output data to a specific target. For the purpose of this project we are writing data the console. To achieve this we have a sub module, called sink-console that implements sink-api interface. The abstraction of sink-api along with it's implementation, sink-console here, is organized as sub modules to make it easier for the users of this API to introduce new custom sinks in future. 

### For example,
If we need data to be written to a file sink in future then we just need to create another sub module, say sink-file, inside the sink module. This will make sure that we are not tampering with the existing top level project structure. Whatever happens remains inside the sink module. It's just a matter of adding a new folder inside sink module.


## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites

Following programs are required to build and run the project -

* Java 8
* Maven

### Installing and running

* Download the project bundle : wget https://www.dropbox.com/s/0sovoj4ie7p3zwi/intercom-round1.zip
* Unzip the bundle : unzip intercom-round1.zip
* Go inside the project root : cd intercom-round1
* Build the project : mvn clean package
* Run the project : java -cp application/target/application.jar org.myorg.intercom.round1.application.Application

## Built With

* [Java](https://www.oracle.com/technetwork/java/javase/overview/java8-2100321.html) - The framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [Guice](https://github.com/google/guice) - The DI framework used
* [Jackson](https://github.com/FasterXML/jackson-databind) - The data-binding framework used
