package org.myorg.intercom.round1.utils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Customer {

	private int user_id;
	private String name;
	private String latitude;
	private String longitude;

	public Customer(@JsonProperty(value = "latitude", required = true) String latitude,
			@JsonProperty(value = "user_id", required = true) int user_id,
			@JsonProperty(value = "name", required = true) String name,
			@JsonProperty(value = "longitude", required = true) String longitude) {
		super();
		this.latitude = latitude;
		this.user_id = user_id;
		this.name = name;
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString() {
		return "Customer [latitude=" + latitude + ", user_id=" + user_id + ", name=" + name + ", longitude=" + longitude
				+ "]";
	}
}