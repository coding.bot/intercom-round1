package org.myorg.intercom.round1.utils;

import java.util.TreeMap;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Inject;
import com.google.inject.name.Named;

/**
 * 
 * @author jarvis
 *
 */
public class Utilities {

	private final static Logger logger = LoggerFactory.getLogger(Utilities.class);
	
	@Inject
	@Named("source_lat")
	private double sourceLat;
	@Inject
	@Named("source_lon")
	private double sourceLon;

	/**
	 * 
	 * @param sourceLat
	 * @param sourceLon
	 * @param targetLat
	 * @param targetLon
	 * @return
	 */
	public double distance(double targetLat, double targetLon) {
		double theta = sourceLon - targetLon;
		double dist = Math.sin(Math.toRadians(sourceLat)) * Math.sin(Math.toRadians(targetLat))
				+ Math.cos(Math.toRadians(sourceLat)) * Math.cos(Math.toRadians(targetLat))
						* Math.cos(Math.toRadians(theta));
		dist = Math.acos(dist);
		dist = Math.toDegrees(dist);
		dist = dist * 60 * 1.1515;
		dist = dist * 1.609344;
		return dist;
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 */
	public static Properties loadConfigs() throws IOException {
		Properties properties = new Properties();
		try (InputStream resourceStream = Utilities.class.getClassLoader()
				.getResourceAsStream("application.properties")) {
			properties.load(resourceStream);
		} catch (IOException e) {
			logger.error("Exception while loading config file", e);
			throw e;
		}
		return properties;
	}
	
	/**
	 * 
	 * @param customerData
	 * @return
	 */
	public TreeMap<Integer, String> processData(ArrayList<Customer> customerData) {
		TreeMap<Integer, String> nearByCustomers = new TreeMap<>();
		customerData.stream()
				.filter(customer -> distance(Double.parseDouble(customer.getLatitude()),
						Double.parseDouble(customer.getLongitude())) <= 100)
				.forEach(filteredCustomer -> nearByCustomers.put(filteredCustomer.getUser_id(),
						filteredCustomer.getName()));
		return nearByCustomers;
	}
}