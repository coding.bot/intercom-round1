package org.myorg.intercom.round1.application;

import java.io.IOException;

import com.google.inject.name.Names;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;

import org.myorg.intercom.round1.sink.api.Sink;
import org.myorg.intercom.round1.source.api.Source;
import org.myorg.intercom.round1.source.file.FileSource;
import org.myorg.intercom.round1.utils.Utilities;
import org.myorg.intercom.round1.sink.console.ConsoleSink;

/**
 * 
 * @author jarvis
 *
 */
public class WiringModule extends AbstractModule {

	@Override
	protected void configure() {
		bind(Source.class).to(FileSource.class);
		bind(Sink.class).to(ConsoleSink.class);
		try {
			Names.bindProperties(binder(), Utilities.loadConfigs());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Provides
	ObjectMapper provideCustomerObjectMapper() {
		ObjectMapper customerObjectMapper = new ObjectMapper();
		customerObjectMapper.enable(JsonParser.Feature.STRICT_DUPLICATE_DETECTION);
		customerObjectMapper.enable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		customerObjectMapper.enable(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES);
		customerObjectMapper.enable(DeserializationFeature.FAIL_ON_NULL_CREATOR_PROPERTIES);
		customerObjectMapper.enable(DeserializationFeature.FAIL_ON_MISSING_CREATOR_PROPERTIES);
		return customerObjectMapper;
	}
}