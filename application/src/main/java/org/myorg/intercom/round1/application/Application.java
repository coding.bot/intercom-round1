package org.myorg.intercom.round1.application;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.inject.Guice;
import com.google.inject.Injector;

import org.myorg.intercom.round1.sink.api.Sink;
import org.myorg.intercom.round1.utils.Utilities;
import org.myorg.intercom.round1.source.api.Source;

/**
 * 
 * @author jarvis
 *
 */
public class Application {

	private final static Logger logger = LoggerFactory.getLogger(Application.class);

	public static void main(String[] args) throws Exception {
		Application app = new Application();
		app.processData();
	}

	/**
	 * Process the data being read from the source and write output into the sink
	 * 
	 * @throws IOException
	 */
	private void processData() throws IOException {
		final Injector injector = Guice.createInjector(new WiringModule());
		final Sink sink = injector.getInstance(Sink.class);
		final Source source = injector.getInstance(Source.class);
		final Utilities utilities = injector.getInstance(Utilities.class);
		try {
			sink.write(utilities.processData(source.read()));
		} catch (IOException e) {
			logger.error("Exception while running the application", e);
			throw e;
		}
	}
}